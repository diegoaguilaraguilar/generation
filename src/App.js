import React, { Component } from 'react';
import YourComponent from './YourComponent';

export default class App extends Component {
  render() {
    return (
      <div>
        <YourComponent/>
      </div>
    );
  }
}
