const goldStar = {
  path: 'M 125,5 155,90 245,90 175,145 200,230 125,180 50,230 75,145 5,90 95,90 z',
  fillColor: 'yellow',
  fillOpacity: 0.8,
  scale: .08,
  strokeColor: 'gold',
  strokeWeight: 2
};

export const clearMarkers = markers => {
  markers.forEach(marker => marker.setMap(null));
}

export const createMarkers = (googleMaps, map, stores, favorites, mode)  => {
  let Marker = googleMaps.Marker;
  let markers = stores.map(store => {
    let {Name, Address, location} = store;
    let markerObject = {
      map,
      position: location,
      title: `${Name}: ${Address}`
    };
    let isFavoriteStore = !! favorites.filter(favorite => favorite.Name === Name).length;
    if (mode === 'favorites' || isFavoriteStore) {
      markerObject.icon = goldStar;
    }
    return new Marker(markerObject);
  });
  return markers;
}

