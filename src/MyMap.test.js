import React from 'react';
import MyMap from './MyMap';
import renderer from 'react-test-renderer';
import { mount } from 'enzyme';

const stores = [
  {Name: 'WeWork Varsovia', Address: 'Varsovia 36, Col. Juárez, 06600 Ciudad de México', location: {lat: 19.424555, lng: 99.16774570000001}},
  {Name: 'Pescadito', Address: 'Estocolmo 29, Juárez, 06600 Ciudad de México', location: {lat: 19.4261309, lng: -99.16620460000001}}
];

test('Properly renders component',  () => {
  const component = renderer.create(
    <MyMap 
      stores={stores}
      favorites={[]}
      mode={'all'}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});