import React, { Component } from 'react';

import MyMap from './MyMap';
import StoresList from './StoresList';
/*
* Use this component as a launching-pad to build your functionality.
*
*/
export default class YourComponent extends Component {

  constructor(props) {
    super(props);
    this.state = {
      stores: [],
      favorites: [],
      mode: 'all'
    }
  }

  render() {
    let {stores, favorites, mode, hover} = this.state;
    return (
      <div style={divStyle} className='row'>
        <MyMap stores={stores} favorites={favorites} mode={mode} hover={hover} />
        <StoresList
          stores={stores}
          favorites={favorites}
          mode={mode}
          toggleMode={::this.toggleMode}
          alterFavorites={::this.alterFavorites} />
      </div>
    );
  }

  alterFavorites(store, include) {
    let {favorites} = this.state;
    let found = favorites.find(favorite => favorite.Name === store.Name);
    if (include && !found) {
      this.setState({favorites: favorites.concat(store)});
    } else if (!include) {
      let newState = {favorites: favorites.filter(favorite => favorite.Name !== store.Name)};
      if (newState.favorites.length == 0) {
        newState.mode = 'all';
      }
      this.setState(newState);
    }
  }

  toggleMode() {
    let {mode} = this.state;
    mode = mode === 'all' ? 'favorites' : 'all';
    this.setState({mode});
  }  

  componentDidMount() {
    fetch('../stores.json')
      .then(response => response.json())
      .then(stores => {
        this.setState({stores});
      });
  }
}

var divStyle = {
  border: 'red',
  borderWidth: 2,
  borderStyle: 'solid',
  padding: 20,
  height: '800px'
};