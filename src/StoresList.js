import React, { Component } from 'react';
import PropTypes from 'prop-types';

class StoresList extends Component {

  render() {
    let {mode} = this.props;
    let modeLabel = mode === 'all' ? 'View favorites' : 'View all';
    let stores = mode === 'all' ? this.props.stores : this.props.favorites;
    return <div className={'col-sm-12 col-md-6'} style={{height: '100%', overflowY: 'auto', float: 'left'}}>
      <div>
        <button 
          className='btn btn-sm btn-primary pull-right'
          style={{textTransform: 'capitalize'}}
          onClick={::this.props.toggleMode}
          disabled={this.props.favorites.length == 0}>
            {modeLabel}
          </button>
      </div>
      <div style={{marginTop: '20px'}}>
        <table className='table table-responsive table-hover'>
         <thead>
          <tr>
             <th>Name</th>
             <th>Address</th>
             <th>Favorite </th>
          </tr>
         </thead>
         <tbody>
          {stores.map((store, index) => {
            return <tr key={index}>
             <td>{store.Name}</td>
             <td>{store.Address}</td>
             <td><input type='checkbox' onChange={event => ::this.onFavoriteClick(event, store)} checked={::this.isFavorited(store)}/> </td>
          </tr>
          })}
         </tbody>
        </table>
      </div>
    </div>
  }

  isFavorited(store) {
    let {Name} = store;
    let {favorites} = this.props;
    let found = favorites.filter(favorite => favorite.Name == Name).length;
    return !! found;
  }

  onFavoriteClick(event, store) {
    this.props.alterFavorites(store, event.target.checked);
  }
}

StoresList.propTypes = {
  stores: PropTypes.array.isRequired,
  favorites: PropTypes.array.isRequired,
  mode: PropTypes.string.isRequired,
  alterFavorites: PropTypes.func.isRequired,
  toggleMode: PropTypes.func.isRequired
}

export default StoresList;