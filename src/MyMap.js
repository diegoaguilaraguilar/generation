import React, { Component } from 'react';
import PropTypes from 'prop-types';
import {createMarkers, clearMarkers } from './google-maps/utils';

const API_KEY = 'AIzaSyCVH8e45o3d-5qmykzdhGKd1-3xYua5D2A';

class MyMap extends Component {

  constructor(props) {
    super(props);
    this.state = {
      markers: []
    };
  }

  render () {
    return <div style={{height: '100%'}} id='map' className={'col-sm-12 col-md-6'}/>
  }

  componentWillMount() {
    const setupMapScript = document.createElement('script');
    const loadGoogleMaps = document.createElement('script');

    setupMapScript.type = 'text/javascript';
    setupMapScript.innerHTML = `function initMap() {window.map = new google.maps.Map(document.getElementById('map'), {center: {lat: 19.432608, lng: -99.133209},zoom: 11, streetViewControl: false, mapTypeControl: false});}`;
    loadGoogleMaps.src = `https://maps.googleapis.com/maps/api/js?v=3.27&key=${API_KEY}&callback=initMap`;
    loadGoogleMaps.async = true;

    document.body.appendChild(setupMapScript);
    document.body.appendChild(loadGoogleMaps);
  }


  componentWillReceiveProps(nextProps) {
    let interval;
    let modeBefore = this.props.mode;
    let {mode} = nextProps;
    let renderingStores = mode === 'all' ? nextProps.stores : nextProps.favorites;
    let storesChange = this.props.stores.length !== nextProps.stores.length;
    let favoritesChange = this.props.favorites.length !== nextProps.favorites.length;
    let modeChange = modeBefore !== mode;
    interval = setInterval(() => {
      // Due to async google maps lib loading (and outside of React component scope),
      // attempt markers loading until is defined
      if (!window.hasOwnProperty('google')) {
        return;
      }
      // Preserving if in case of more props being added in the future
      if (storesChange || favoritesChange || modeChange) {
        let googleMaps = window.google.maps;
        clearMarkers(this.state.markers);
        let markers = createMarkers(googleMaps, window.map, renderingStores, nextProps.favorites, mode);
        this.setState({markers});
        clearInterval(interval);
      } else {
      }
    }, 100);
  }
}

MyMap.propTypes = {
  stores: PropTypes.array.isRequired,
  favorites: PropTypes.array.isRequired,
  mode: PropTypes.string.isRequired,
  hover: PropTypes.shape({
    Name: PropTypes.string.isRequired,
    Address: PropTypes.string.isRequired
  })
}

export default MyMap;