import React from 'react';
import StoresList from './StoresList';
import renderer from 'react-test-renderer';
import { shallow } from 'enzyme';

const stores = [
  {Name: 'WeWork Varsovia', Address: 'Varsovia 36, Col. Juárez, 06600 Ciudad de México'},
  {Name: 'Pescadito', Address: 'Estocolmo 29, Juárez, 06600 Ciudad de México'}
];
const favorites = [];
const alterFavorites = () => {};
const toggleMode = () => {};

const wrapper = shallow(<StoresList 
  stores={stores}
  favorites={[]}
  mode={'all'}
  alterFavorites={alterFavorites}
  toggleMode={toggleMode}
/>);

test('Properly renders component',  () => {
  const component = renderer.create(
    <StoresList 
      stores={stores}
      favorites={[]}
      mode={'all'}
      alterFavorites={alterFavorites}
      toggleMode={toggleMode}
    />
  );
  let tree = component.toJSON();
  expect(tree).toMatchSnapshot();
});

test('Contains a valid list of stores', () => {
  let stores = wrapper.find('tbody tr');
  expect(stores).toHaveLength(2);
});

test('Disables view favorites with empty favorites prop', () => {
  let button = wrapper.find('button').first();
  expect(button.text()).toEqual('View favorites');
  expect(button.prop('disabled')).toBe(true);
});

test('Passing stores in favorite renders only those elements and enables button', () => {
  const wrapper = shallow(<StoresList 
    stores={stores}
    favorites={[{Name: 'Pescadito', Address: 'Estocolmo 29, Juárez, 06600 Ciudad de México'}]}
    mode={'favorites'}
    alterFavorites={alterFavorites}
    toggleMode={toggleMode}
  />);
  let storeRows = wrapper.find('tbody tr');
  expect(storeRows).toHaveLength(1);
  let button = wrapper.find('button').first();
  expect(button.text()).toEqual('View all');
  expect(button.prop('disabled')).toBe(false);
}) 