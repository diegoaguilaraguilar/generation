const request = require('request');
const stores = require('./store_directory');
const API_KEY = 'AIzaSyCVH8e45o3d-5qmykzdhGKd1-3xYua5D2A';
const geolocationUrl = 'https://maps.googleapis.com/maps/api/geocode/json';
const fs = require('fs');
const writeStream = fs.createWriteStream('./stores.json');


function includeLocation(store) {
  let qs = {
    // Geocoding results seem more successful by removing ZIP code
    // following regex attempts to ZIP codes present in addresses in distinct ways
    // like with or with out dot, multiple spaces before ZIP number, etc
    address: store.Address.replace(/C\.?P\.? *\d+/i, ''),
    key: API_KEY
  };
  console.log('Will require', store.Name);
  return new Promise((resolve, reject) => {
    setTimeout(function() {
      request.get(geolocationUrl, {qs, json: true}, function (err, response, body) {
        if (err) {
          console.error('Error retrieving store', store.Name, err);
          return reject(err);
        } else {
          if (body.status == 'OK') {
            let {location} = body.results[0].geometry;
            store.location = location;
            console.log('Resolving store', store);
            resolve(store);
          } else {
            resolve(store);
          }
        }
      })
    }, 350);
  });
}

function retrieveWholeStoresAddresses() {
  let promises = stores.map(includeLocation);
  Promise.all(promises)
    .then(stores => {
      console.log('Resolved all stores');
      let storesJSON = JSON.stringify(stores, null, 2);
      writeStream.write(storesJSON);
      writeStream.end();
    })
    .catch(console.error);
}

retrieveWholeStoresAddresses();

