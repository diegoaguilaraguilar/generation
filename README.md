Generation Take-Home Coding Challenge
=================================

# Overview

My solution approach to the coding challenge consists in creating two components per each main visual functionality:

* See stores
* List stores

According to guidelines not a single dependency has been included for the solution and all implementation is done by using vanilla JS.

Solution approach also focuses on simplicity over complex solutions, this may have a minor impact against fancy features being implemented but ensures that user can interact with an intuitive interface keeping balance of coding simplicity and invested time, in additon and following same argument, I decided to skip implementing a backend or data persistance solution.

The complete user stories have been implemented:

* As a user I can see a map of México City
* As a user I can see pinpoints per each known store
* As a user I can add/remove favorite stores

# Implementation details

### `geocode.js`

Due to API rate limits, I decided to manually call Google Maps Geocoding API in a separate script with sequential API calls in a slow pace (a 350 milliseconds timeout). Otherwise, any other implementation in frontend would impact on user's experience or would make necessary to persist location data.

A relevant decision was to drop ZIP code's off from stores addresses, this regex was used for that purpose `/C\.?P\.? *\d+/i`. This way I could ensure a greater number of addresses being resolved to a location.

Just for convenience, `request` module's been used in this script.

### `YourComponent`

Due the not including any data layer library, this component behaves as main state persistance, keeping in its state the running application data and interacting with both `MyMap` and `StoresList`.

Also, it's in charge of loading the generated stores list (see it's `componentDidMount` lifecycle method).

### `MyMap`

This React component performs manual loading of the Google Maps library, by appending script tags. Due to async maps library loading, we can't trust on having a defined reference to either google or map globals. This is why a set of attepmt retries to interact with the library on props changes has been implemented inside `componentWillReceiveProps` using `setInterval`.

The render method of this component only makes sure to return a div where a Google Map will be rendered. Stores markers are kept as a state properties to ensure markers can be updated.

### `StoresList`

Receives the stores array and the current favorited stores as props, it also allows user interaction to visually filter either all stores or just the ones being favorited.

# Testing

Snapshot and component rendering assert tests has been included using Jest and enzyme. Just run `npm test` to run them.

# Future work

Following implementation details or improvements could be performed in order to provide a better UI/UX:

### Markers rendering performance

Right now, the whole markers are being cleared out and rendered back on any store or favorite change, this produce a minor but signicative impact on the UI, experienced as a blink on each change.

Most immediate strategy is to use a better data structure to keep track of the stores instead of an array, which forces to clear the whole markers and iterate over the whole array until asserting a favorite. 

### Data persistance

Using local storage for keeping track to avoid fetching the stores list and keeping track of favorited is the most immediate solution. Also a simple backend with a remote API using any DB would be an option.

### Dismiss Bootstrap styles use
A minor set of CSS classes and using [styled components](https://github.com/styled-components/styled-components) for styling would be more appropiate.

### Features

* Add store
* Store hovering with visual effect on map